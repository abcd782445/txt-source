「雷肯殿」
「嗯？」
「雷肯殿雖然知道裡面的狀況，但通常沒辦法知道。如果戰鬥中有別的隊伍進來的話，會怎麼樣呢」
「我原本的世界，在冒険者跟魔獸戰鬥的途中闖進去的人，就算被殺了也不能有怨言，有這默認的規則。更別說對戰鬥出手的話，會被烙上〈搶怪混帳〉的印」
「嘿。那麼大家就不會做那種事阿」
「會做的傢伙還是會做」
「這不是沒用嗎」
「能保護自己的，終究只有自己。偷窺別人戰鬥的冒険者，不知道何時會襲擊過來。沒辦法應對的人，就沒辦法在迷宮活得久」
「原來如此」
「對獵物被搶走感到懊悔的話，就只能增強實力到不會被搶走。如果被出其不意地殺掉的話，就代表實力或警戒心不夠」
「果然迷宮就是這種地方呢」
「雖然不知道這世界的迷宮在這方面是怎樣，但如果在別的隊伍戰鬥時不小心闖入的話，就應該馬上離去吧」
「如果我們在戰鬥的時候，有人闖入的話呢？」
「那要看對方了。但是不能大意。好，前面的隊伍下去了，下個魔獸湧出來了。進去吧。別殺了〈赤肌〉」
「好的」

雷肯先進去，阿利歐斯接著進去。

「〈雷擊〉！」

雷電劈哩啪啦地閃出，纏上了六隻魔獸。
雷肯把在前頭的〈黑肌〉的頭砍飛。
阿利歐斯繞到右邊裡頭把〈黑肌〉的頭砍落。
這下剩的有兩隻〈黑肌〉和兩隻〈赤肌〉。

「〈展開〉！」

雷肯的左手現出了〈沃爾肯之盾〉。
四隻魔獸再次開始行動。一隻〈黑肌〉向雷肯前進。以〈沃爾肯之盾〉擋住揮下的斧頭。
另一隻〈黑肌〉向阿利歐斯移動。發出低吟，攻擊過去的雙手大劍，被阿利歐斯輕鬆閃過。
兩隻〈赤肌〉慢慢地向後方移動。
雷肯以盾牌不斷防下魔獸的斧頭。
阿利歐斯不斷閃避雙手劍的斬擊。

沒多久，退到後方的一隻〈赤肌〉，張大了口，發出了呻吟聲。
波喔喔喔喔。
然後，口中生成了光。
暗白色的表皮，染成了淡紅色。
口中的光爆發，化成光之槍襲擊雷肯。

「嗚嗯」

雷肯以〈沃爾肯之盾〉承受那光之槍。
發出了沉重的聲音，盾牌搖晃。

另一隻〈赤肌〉晚了一瞬，才剛張開口，就吐出了光之槍。光之槍擊中了阿利歐斯的腹部，彈開了。
雷肯前方的〈黑肌〉緊接著揮下了斧頭。
雷肯後退半步，閃開斧頭，踏出一大步，橫向揮劍，把魔獸的頭砍飛，然後避開直立的無頭屍體向前突進，把〈赤肌〉的頭砍飛。

至於阿利歐斯，已經把〈黑肌〉一隻和〈赤肌〉一隻屠掉了。

「還好吧？」
「是的。這鎧甲真厲害呢」

阿利歐斯似乎完全沒受到傷害。
是以在尼納耶的深層入手的八目大蜘蛛上位種的甲殻和腹部，還用上女王種的甲殻製作的鎧甲。實際證明了對魔法攻擊有很強的耐性。

「沒預備動作呢」
「是阿。真不可思議。明明先攻擊雷肯的魔獸發出了呻吟，但是攻擊我的魔獸，卻突然擊出了魔法」
「雖然不太清楚，但有一隻準備過的話，第二隻就能省略準備吧」
「或者是，就算不發出呻吟也能準備也說不定」
「說不定有呻吟聲以外的某種預兆。看看下場戰鬥的情況吧」

第二隻〈赤肌〉的魔法攻擊出乎了意料。
但如果是阿利歐斯，應該還是能閃開。應該是為了確認鎧甲的性能，特地以裝甲較厚的部分試著承受。

「好的。話說回來，還挺有威力的呢」
「啊啊。衝擊也強，似乎也有貫穿力。感覺是類型跟〈炎槍〉相似的攻擊」

出了一個寶箱便打開來看看，放了斧頭。
鑑定後，附有〈輕量化〉和〈威力附加〉的恩寵。感覺能賣個高價。

「迷宮外的白幽鬼，也能使用魔法嗎」
「不不，沒那種事。用不了的。這迷宮的白幽鬼是特別的」
「是嗎」
「仔細一想，這魔獸挺麻煩的呢」
「是嗎？」
「雷肯殿和我的劍都有威力，也有速度。也有精密的準度。所以能輕鬆把頭砍飛。但是普通的冒険者沒辦法這樣。畢竟〈黑肌〉的防禦力很高」
「說得也是」
「在跟〈黑肌〉纏鬥時，〈赤肌〉開始從後方以魔法攻擊。這樣一來，是相當大的威脅喔」

五十階層帶的〈黑肌〉，有著等同騎士的鎧甲的硬度。然後，鎧甲上部有突出的部分，覆蓋了頭部這弱點。雷肯和阿利歐斯雖然避開了那突出的部分來攻擊，但到了這階層，魔獸的動作也很快，普通的劍士沒辦法做到相同的事吧。

從魔獸的屍體拔出魔石，雷肯和阿利歐斯向五十三階層下去。