８

篝火的火勢衰弱了下來，雷肯便起身添加柴火。

艾達牢牢地靠著樹睡著。又調整了姿勢。
有問題發生了。

拜魔獸驅散的福，並沒有魔獸接近。然而，宛如要包圍其有效範圍似的，周遭聚集了許多的魔獸。
原本的世界有能驅散魔物的魔法，但若發動過度，就會把魔獸吸引到邊界線附近。這就類似於那種現象。

到了第二天早上，傑尼跟艾文睡醒了。雷肯告知了魔獸聚集的事情，以及推測的原因。

「所謂大量的魔獸，大概有多少呢？」
「兩百左右吧。」
「誒誒誒」
「這是真的嗎。這下子，就不能讓魔獸驅散斷了。」
「說的也是。雖然捨不得魔石，接下來就在覆著魔獸驅散的狀態下移動吧。哎呀，雖然有聽說過用上強力的魔石反而會吸引魔獸，一不注意，就太過勤奮的用上強力的魔石了。」
「在那之後呢。」
「什麼意思，雷肯先生。」
「覆著魔獸驅散移動的話，魔獸就會順著跟上來。抵達城鎮後，城鎮就會受到襲擊。」
「阿⋯⋯」
「要是讓城鎮受到魔獸襲擊，可不知道會有怎樣的懲罰吶。」
「怎麼⋯⋯辦呢」
「總之，先覆著魔獸驅散移動。到了平地再想辦法。」
「我知道了。那就開始行動吧。」
「小妹妹，已經早上了。再不起來就丟下妳不管囉。喂喂，小妹妹」
「嗚、嗚～嗯。早上了嗎？早飯呢？」
「早飯請在移動中各自處理。那麼出發吧。」

雷肯跟昨天一樣，一邊引導著馬車，一邊啃著肉乾和豆子，喝著水。
不久後，馬車停在了平地上。然後，隔了約五百步的距離跟著過來的魔獸們的身姿也能清楚看見了。

「跟兩百比起來，不會少很多嗎？」
「有半數以上的魔獸，不想離開山上的樣子。」
「話說回來，這還真多阿。有百隻左右吧。」
「沒，沒辦法的！就算有十個冒険者，也絶對應付不了。我聽說沃卡有守備隊的樣子。就讓他們打倒吧。」
「途中的村子會全滅喔。而且，也不知道魔石撐不撐得住。雷肯先生，這下怎麼辦才好。」

雖然有很多雷肯不知道正確名稱的魔獸，但沒有一隻有著龐大的身軀或魔力。都是種類類似狼、豬、猿、熊等的相當下級的魔獸。
若是在森林中，就算各個都很弱小，數量大便能造成威脅。
更何況還得守著護衛對象，就變得非常難以戰鬥。
但是，這裡是平地。這種狀況下，可以一時放棄保護護衛對象。
再加上對手也不會企圖逃跑。

「呼嗯。魔獸驅散就那樣持續著。我去去就回。」

語畢，雷肯便衝向了魔獸群。
很快就到達了。
拔劍。然後一個個斬下。
雖然都是沒什麼魔力的雜魚魔獸，但不知道有著怎樣的攻擊手段。既然如此，先手必勝。不給對方攻擊的機會，以速攻殲滅最有效。
不留一匹地殺盡，向死屍的山行禮後，雷肯便跑回了馬車旁。

「魔獸驅散可以停了。出發吧。」
「幾乎沒有被血濺到吶。這實在是。」
「我還是第一次目睹，如此猛烈的劍技。」
「⋯⋯雷肯。不，雷肯先生。你是持有別名的傭兵嗎？」

在雷肯走過艾達面前要到馬車前方時，艾達注意到了雷肯的左手中指上發著光的銀色戒指。

「你原來有戴著這種時髦的戒指阿？」

那戒指是第一天進入森林時從〈收納〉中取出戴上的。
但沒有必要為此說明。

「算是吧。」


９

第二天中午前後雖然抵達了個小村莊，但補充了水並稍作休息後就直接出發了。因為艾達眼睛渙散搖來晃去的，問她是怎麼了，得到的回答是肚子餓了。出於無奈，就分了點肉乾。順帶一提，艾達終究還是坐上了貨物車。傑尼允許了。

太陽西沉時，剛進了山中沒多久，雷肯便查覺到有魔獸接近。

「雷肯先生，怎麼了嗎？」
「有魔獸。十八隻。從前方過來了。停下馬車。迎擊了。」
「了解了。艾文先生！請停下馬車。」
「敵，敵人嗎？在哪裡。」

一行人保持著安靜，等待著敵人接近。艾達在貨物車上拉著弓，緊張地眺望著昏暗的森林前方。
樹木沙沙作響著。

「在上面。」

雷肯開了口。〈生命感知〉不能判別高低，所以在靠近前並不知道敵人是從高處過來。
大量魔獸從頭上降了下來。

「嗚哇，嗚哇」

雖然艾達邊發出悲鳴邊不斷射出箭，但完全碰不到魔獸。
車夫座上的艾文，仔細地看清魔獸的動作，閃避或防住攻擊。
是很像猿猴的魔獸。
大小接近十歳的小孩，全身被黑色的體毛覆蓋，只有臉部是赤紅的。
很敏捷的樣子。

雷肯悠然地屠下了魔獸。
雖然動作有點奇特，但對雷肯的動態視力和攻擊速度來說，只能用緩慢形容。

委託主傑尼躲在馬車裡關緊了窗子，相當安全。並不是能一擊破壊馬車的敵人，而且根本連悠哉攻擊的時機也沒有。
比起猿猴，艾達危險多了。有兩次往雷肯射了箭。第二次時實在是生了氣，直接在空中把箭砍了。
最後砍死的是躍起咬向艾文的猿猴。

艾達好痛好痛的喊著。是被猿猴抓傷或咬傷了吧。終究是沒辦法連那邊都照料到。

「結，結束了嗎？阿，貨物車都被血弄髒了。但是現在時間比較重要。快走吧。」
「阿，傑尼先生。等一下。箭矢，不回收箭矢的話！」

聽到艾達的話，謎團總算解開了。到底把預備的箭矢收在哪，讓人覺得很不可思議。想不到，艾達根本沒有預備。

「艾文先生，請出發吧。」
「齁，齁，齁。」

不聽艾達的懇求，傑尼讓馬車出發了。艾達匆忙地回收了附近的箭後追在馬車後頭。順帶一提，艾達握有的十二支箭矢，有射中猿猴魔獸的數量是零。

第二天的野營在山裡。
突然間，艾達叫了出來。

「魔石！」
「魔石怎麼了。」
「剛剛，明明打倒了赤猿，怎麼沒有回收魔石！」

看來，那就是赤猿。
聽說是迷宮跟森林最普遍的魔獸。至今為止也打倒過，但沒能從那身姿聯想到名字。
話說回來，艾達事到如今又在說些什麼。是沒聽到剛剛傑尼說的「時間比較重要」嗎。

「那也是一筆錢啊！」

雖然對於艾達有沒有資格分到感到疑問，說到底區區赤猿的魔石連特地取出的價值都沒有。這方面，中午打倒的八十隻左右的魔獸剩下的魔石還比較有價值。不過關於中午打倒的魔獸，傑尼已經告訴中途停留的村子的村長，把魔石素材等的一半帶到傑尼的處所，剩下的作為村子的財產也無所謂，既不會有所浪費，死Ｘ屍也不會腐敗造成危害吧。

「吶，現在回去也不行嗎？」

因為艾達實在太吵了，雷肯便提起別的話題。

「艾達，那個脖子上的黃色圍巾，很顯眼。」
「誒？阿，很可愛對吧。嘿嘿。」
「在暗處也看得到。」
「喔，這樣啊。謝啦。」
「會是個記號，讓敵人跟魔獸攻擊。」
「誒？」
「冒険者不會穿戴那種華麗的圍巾。」
「⋯⋯嗚哇哇哇哇！怎麼會這樣。既然如此就早點講阿！嗚哇。所以剛才才會那樣子聚過來嗎。嗚哇。嗚哇。一想起來，這裡那裡都痛起來了！可惡。可惡。」

想著真是個吵鬧的女的，便想起來，華麗外觀的冒険者也不是沒有。

是個叫做〈染血〉蘭希的女冒険者。有著燃燒般的真紅頭髮，身穿鮮Ｘ血般的配色的皮鎧。雙手手指上閃耀著亮晶晶的寶玉。實在是個華麗的女人。把魔獸引過來再用大劍斬Ｘ殺，總是濺了滿身的血。是個非常美麗的女人，所以也引來了大量的男人。而全員的心靈也都被徹底地摧殘了。

突然間，懷念起了原本的世界。

仰望了天空，但滿是繁星沒有月亮。那個大而明亮的月亮，在這個世界並不存在。
這讓人感到非常的寂寞。