看來沒有想過自己的火焰吐息能被完美防禦下來，紅龍臉上的表情好像「真的假的？」這樣說。

「事已至此只能戰鬥了啊！」
「沒錯！」
「呃～嗯。確實，我記得紅龍一旦放出吐息，就會有一段時間放不出來了。」
「那麼，就趁現在一口氣打倒吧！」

正因為剛才躲在一起，被艾莉婭的紅姫守護了。
但是，如果在散開的狀態下吐出來的話，這一次，我和珂露雪肯定會受到直擊的吧！

穿過被燒得焦黑的澤恩們身旁，向紅龍衝鋒。

「噶啊啊啊啊⋯⋯！」

對著衝在最前頭的珂露雪，紅龍揮下著前爪。

「拓～！」

以毫釐之差躲過銳利的爪擊，珂露雪抓住了紅龍揮空的前爪，

「汰～～呀啊～！」

竟然豪爽地順手扔了出去。

「嗚⋯⋯！？」

巨大的身軀在空中飛舞，背向地面摔了下來。
那個衝擊連腳下晃動了。

喂～～ 喂，雖說是利用了對方的力量，但把紅龍扔出去之類的，是何等的怪力啊！

我一邊驚嘆著，一邊撲向背朝地倒在地上的火焰之龍。
抄起維納斯朝那長長延伸的脖子上就是一擊。

「⋯⋯果然是很硬啊！」
「嘎啊啊⋯⋯！？」

本來是打算砍掉頭顱的一擊，但是被龍種特有的堅硬的鱗片擋住了，所以功虧一簣。

儘管如此，還是成功地撕裂了鱗片，給予了一定程度的傷 害。

鮮血噴涌而出，紅龍因疼痛而暴亂掙扎。

在毫無章法來回揮舞尖牙和利爪之下，我只能迅速跳開迴避。

「給我老實點啊！」
「～ ～ ～咕！？」

被珂露雪踢中後腦勺，龍頭正面猛撞擊地面。

我馬上過去以斬擊進行關照。

「厲，好厲害⋯⋯對著BOSS怪物⋯⋯」
「該不會壓制下來了⋯⋯？」

聽見了澤恩手下兩人嘟噥的聲音。

但是，不愧是BOSS怪物。
雖然已經用神劍砍了兩次，但還沒有死。

「啊！翅膀⋯⋯」

紅龍一下把背後的翅膀張開，就開始扇動起來了。

周圍吹起了暴風，差點就這樣被吹跑了。

等我回過神來，火焰龍已使其巨體浮上半空。

「不妙⋯⋯這樣一來攻擊就夠不到了！」

緊接著，紅龍以猛烈的勢頭滑行撞過來。

因此馬上躺下頭朝上，鋒利的後腳爪剛好擦過。

「怎麼辦⋯⋯？把劍扔過去⋯⋯不，果然也不可行呢！」
『這個，要是把我扔出去的話，我可饒不了你哦？』

沒有從這邊開始的攻擊手段。

如果是艾莉婭的紅姫，即使可以生成火球進行攻擊，但是對方是完全無效化火炎和熱量的紅龍。

那個時候，大概是吐息的充填完成了吧，紅龍的喉嚨再次鼓了起來。

「兩人快躲到我身後⋯⋯！」

艾莉婭迅速地跑了過來。

「啊！珂露雪！？」

但是，珂露雪獨自離開了我們身邊，不知在考慮些什麼。

「這樣下去的話，澤恩君他們就⋯⋯！」

看來是為了幫助失去了〈炎熱耐性〉的斗篷的他們。

這種狀況本來是他們自作自受。
放著不就好了⋯⋯雖然這樣想，但無法做到這一點那就是他的性格吧！

「沒事！畢竟我的身體比一般人結實得多⋯⋯！」

珂露雪站著庇護著他們，大聲回答。

知道他的想法，澤恩他們非常吃驚。

不管怎麼說，竟然打算用自己的身體來承受阻擋紅龍的火焰。

確實，珂露雪的身體偏離常人地結實。
筋骨力和體力自不必說，抗擊打的強度也相當大。
或許還有對火炎和熱量的抵抗力，即使接受出沒於該層樓敵人的火之息，也只是輕微的燒傷而已。

即便如此，若是要正面挨個正著，果然是不可能平安無事的。

「⋯⋯別想得逞。」

我把維納斯高高掄起、舉過頭頂，

『等，我主，難道真的──』
「噢啦啊啊⋯⋯！」
『──投出噠啊啊⋯⋯！？』

全力投了出去。

為了放出吐息，紅龍只能一直滯空不動，而且有必要張大嘴巴。
因此。

神造的劍，剛好飛進火焰即將噴出的口腔中。

「～～～～～～污污污～～～！？」

保護炎龍的硬鱗，到底是不會連口腔內都覆蓋了吧！

維納斯漂亮地扎進喉嚨深處的顎垂處。
受到預想之外的劇痛紅龍慌忙向天射出熾熱的火淖液。

然後背對著這邊，像要逃一般地離開了。

稍微晚了一點，升起的火焰像雨一樣傾倒下來。
咔鐺地，一聲響起。

大概是跟吐息一起吐出來了吧，維納斯也掉下來了。

「⋯⋯呼。看來總算把它趕走了。」

雖說只是賭一下碰碰運氣，但成功真是太好了。

『喂～吼拉！不是說叫你不要扔了嗎！』

不好～抱歉。
但是，最後也贏了吧！
何況放任吐息就那樣被放出來的話，珂露雪一定會受到很大的傷害。

『咕奴奴奴⋯⋯！』

由於維納斯多少也明白這一點，因此無法反駁而懊悔不已。

『那麼如果能讓我的臉被珂露雪圓滾滾有彈性的屁股填滿的話，就可以考慮原諒你吧！』

那需要得到本人的許可啊！